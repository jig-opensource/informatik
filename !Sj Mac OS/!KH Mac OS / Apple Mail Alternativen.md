# Einleitung

Als Windows-User staune ich ja sehr über Apple Mail, denn Standardfunktionen werden nicht unterstützte, Kundenreklamationen ignoriert Apple seit jahren und trotzdem sind die Kunden von Apple begeistert.

**Vielleicht hilft zur sachlicheren Beurteilung von Apple**, dass Apple sehr viel stärker als andere Unternehmen alles tun, um keine Steuern zu zahlen: zwischen 2003 und 2016 dürfte Apple mehr als 100'000'000'000$ Gewinn gemacht haben, in der EU bezahle Apple aber 0.005% Steuern. 
Man könnte getrost behaupten, dass sich Appleauch nur zu 0.005% für Strassen, Schulen, Spitäler, Kindergärten, Ausbildung, etc. interessiert. Ich würde behaupten, dass Apple Kinder hasst, denn die Firma tut mit Hilfe der Politik alles, um für die nächste Generarion nur verbrannte Erde übrig zu lassen.


**Microsoft ist nicht viel besser**, aber sie bezahlten in der gleichen Periode immerhin noch erund 600x mehr Steuern.



# Apple Mail Alternativen

Stöbert man in Apple Mail Foren, findet sich sehr viel Frust, weil Apple Standard Mail-Funktionen nicht zur Verfügung stellt und Apple Reklamationen seit vielen Jahren ignoriert. Als Schwächen werden u.A. genannt:

- Nur grundlegende E-Mail-Verwaltung.
- Fehlende Anpassungsoptionen wie Themes, Farben oder Hintergrund.
- Fehlende unverzichtbare Funktionen wie Schlummern, E-Mail-Zeitplanung oder E-Mail-Verfolgung.
- Synchronisierungsprobleme **(!)**
- Keine nativen Integrationen mit Drittanbieter-Apps


Für unsere Arbeit ist wichtig:
1. Unsere Mails haben potentiell sehr sensible persönliche Informationen.
   - Deshalb gehören diese Mails ganz sicher nicht auf die Server von Apple, was aber mit Apple Mail nicht unterbunden werden kann.
2. Die Konfiguration einer Antwort-Adresse ist sehr nützlich.


Deshalb nachfolgende Liste mit alternativen Mail-Programmen für Apple.

Quellen der Test-Seiten:
1. [6 Beste Alternativen zu Apple Mail für Windows im Jahr 2023](https://www.getmailbird.com/de/windows-alternativen-zum-besten-mac-e-mail-client/)
2. [Apple Mail: Die sechs besten Alternativen für Emails am Mac](https://www.maclife.de/ratgeber/apple-mail-sechs-besten-alternativen-emails-am-mac-10072700.html?page=1)


## Mailbird – bietet eine hervorragende Verwaltung mehrerer Konten und beeindruckende Produktivitätsfunktionen
- https://www.getmailbird.com/de/
- Wurde in den letzten Jahren von Zeitschriften als eines der besten Mail-Programme bewertet
- Unterstützt Windows und Mac OS 
- [Preise](https://www.getmailbird.com/de/pricing/?utm_location=CH)
  - Persönliche Lizenz: einmalig 50€ (50% Rabatt)
- Funktionen
  - [Funktionen #1](https://www.getmailbird.com/de/features/)
  - [Funktionen #2](https://features.getmailbird.com/)
  - Ev. für Apple Kunden besonders interessant: Dunkles Farbschema
  - Definition einer Antwort E-Mail Adresse: **noch nicht klar, Supportanfrage gestellt**

## Outlook – harmoniert gut mit Microsoft Office
Unterstützt unter Mac OS keine Definition der Antwortadresse

- App Store: https://apps.apple.com/de/app/microsoft-outlook/id985367838?mt=12


## eM Client – bietet eine ausgezeichnete Kontaktdatenverwaltung
Unterstützt die Definition der Antwortadresse vermutlich **nicht**, eine Supportanfrage wurde geschickt.
- Homepage: https://www.emclient.com/
- Unterstützt Windows und Mac OS 
- Gratis für die private, nicht kommerzielle Nutzung
  - Jedoch etwas eingeschränkte [Funktionen](https://www.emclient.com/pricing)
- [Preise](https://www.emclient.com/pricing)
  - Einmalig 60.-
  - Mit lebenslangen Aktualisierungen: 140.-
  - 30 Tage Geld-zurück Garantie


## Thunderbird – eine bewährte kostenlose plattformübergreifende Alternative
- Kostenlos, open-source
- Dowload: https://www.thunderbird.net/en-US/thunderbird/all/
- Unterstützt (mind. unter Windows) das Definieren der Antwortadresse. 
  Ziemlich sicher auch unter Mac OS.

Testresultat (Quelle 2)
- Thunderbird ist ein hervorragender Mac Mail Client, da es Open Source ist, Erweiterungen von Drittanbietern unterstützt und eine Benutzeroberfläche mit Registerkarten bietet, mit der Sie in einem einzigen Fenster mehr erreichen können. Thunderbird verfügt über einen integrierten Webbrowser, sodass Sie Links folgen können, ohne die Anwendung zu verlassen.
- Zum Zeitpunkt des Schreibens sind über tausend Thunderbird-Erweiterungen verfügbar. Zu den beliebtesten Erweiterungen gehören Lighting, ein vollständig in Thunderbird integrierter Kalender, Enigmail, der OpenPGP-Nachrichtenverschlüsselung und -authentifizierung für Thunderbird bietet, und Quicktext, eine Erweiterung, mit der Sie Vorlagen erstellen können, die einfach in Ihre eigenen E-Mails eingefügt werden können.
- Mit diesen und vielen anderen Erweiterungen können Sie sich Thunderbird wirklich zu eigen machen und es perfekt an Ihren Workflow anpassen. Wenn Sie bei der Thunderbird-Anpassung noch weiter gehen wollen, können Sie auch Thunderbird Themes erkunden und Ihren Email Client neu einkleiden.
- Schlechte Integration mit macOS


## Mailspring – einfach einzurichten und für mehrere E-Mail-Konten zu verwenden
Unterstützt die Definition der Antwortadresse vermutlich **nicht**
- Unterstützt Windows und Mac OS
- Homepage: https://www.getmailspring.com/
- Gratisversion mit eingeschränkten Funktionen
- Preise: https://www.getmailspring.com/pro
  - 8$ / Monat, meines Erachtens 10x zu teuer für ein einfaches Mailprogramm


## Airmail
- Homepage: https://airmailapp.com/
- Preise: https://airmailapp.com/#pricing
  - Airmail Pro: 30$/Jahr
- **Unterstützung einer Antwortadresse:** unbekannt

## Postbox
- Unterstützt Windows und Mac OS
- Homepage: https://www.postbox-inc.com/
- Preise: https://www.postbox-inc.com/store/pricing
  - Einmalige Kosten: 46$
- Funktionen: https://www.postbox-inc.com/features
- **Unterstützung einer Antwortadresse:** unbekannt

## Spark
- Homepage: https://sparkmailapp.com/?
- Preise: https://sparkmailapp.com/plans-comparison
  - Gratisversion eingeschränkt
  - 60/Jahr
- **Unterstützung einer Antwortadresse:** unbekannt


## Canary Mail
- Homepage: https://canarymail.io/
- Preise: https://canarymail.io/pricing.html
  - Gratisversion verfügabr, Funktionen: https://canarymail.io/pricing.html
  - Pro: 20$/Jahr :-)
- **Unterstützung einer Antwortadresse:** unbekannt



## Unibox 
- App Store: https://apps.apple.com/de/app/unibox/id702816521?mt=12
- Wird wohl nicht mehr weiter entwickelt / unterstützt
















