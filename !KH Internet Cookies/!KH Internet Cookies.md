# Knowhow zum Thema Cookies

## Sehr wichtige Grundlagen
- Cookies werden **nicht** nur in der Hauptseite (z.B. https://rogerliebi.ch/) definiert,\
sondern **jede einzelne Seite definiert unabhängig Cookies!**
  - Das heisst:\
  https://rogerliebi.ch/person/ könnte YouTube-Cookies haben\
  obwohl https://rogerliebi.ch keine solchen Cookies hat.

- **Wenn eine Webseite bisher fremde Cookies nützte und es neu nicht mehr tut**\
(also z.B. früher YouTube-Cookies im Einsatz hatte aber in der neuesten Webseitenversion nicht mehr),\
**dann haben frühere Internetseiten-Besucher die alten Cookies immer noch im Browser!**

- [ ] ❓ Frage: Was passiert mit alten 3rd party Cookies, wie z.B. von YouTube, die frühere Webseitenbesucher noch haben?
  - Muss RogerLiebi.ch alte YouTube-Cookies löschen
  - Oder kann man das Löschen dem User überlassen werden?
  - Oder muss RogerLiebi.ch aktiv darauf hinweisen, wenn ein User noch YouTube-Cookies hat?\
  (dann kann die Webseite die Cookies wohl auch grad löschen)

  
### ⓘ Praxis bei Akros, Noser Engineering & Co 
- Wir haben unsere früheren Webeseitenbesucher nicht aktiv über allenfalls alte, aber noch aktive Cookies informiert
- Aber neue Cookies werden natprlich erst nach Zustimmung gesetzt



## Risiken & Gefahren

Im Zus'hang mit Cookies haben wir diese Gefahren:
1. Bei der Umstellung auf DSGVO konforme Webseiten kann es passieren, dass man alte Webseiteninhakte übersieht, die Cookies erzeugen
2. Es kann passieren, dass neue Webseiteninhalte Cookies erzeugen, weil
    - Fehler passieren (z.B. copy / paste eines 'unglücklichen' Links) 
    - oder Autoren nicht merken, dass mit der Nützung einer Editor-Funktion eine DSGVO-Verletung entsteht


## Konzepte & Ideen zum Schutz vor DSGVO-Verletzungen

Um unsere Webseitenbesucher zu schützen und damit wir die DSGVO nicht verletzen, gibt es verschiedene Lösungsansätze
1. Obwohl wir keinen (meist kostenpflichtigen) Coockie-Manager benötigen, nützen wir trotzdem einen solchen.
    - Sollte die Webseite aus irgendwelchen Gründen irgendwann ein unerlaubtes Cookie erzeugen, dann bliebe es automatisch unter Kontrolle
2. Automatisierte Prüfung der Webseite auf Cookies
    - Es ist recht einfach, automatisiert _eine_ Webseiten-URL zu analysieren und festzustellen, welche Cookies erzeugt werden
    - Aber es ist knifflig, von einer Webseite automatisch alle existierenden Webseiten-URLs zu erkennen, also z.B.:
      - https://rogerliebi.ch/person/
      - https://rogerliebi.ch/f21462-wie-sollen-wir-uns-in-der-endzeit-verhal/
      - https://rogerliebi.ch/r275-02-folge-2-auftrag-gebot-an-timotheus-1-4-11/
      - …
    - Es ist zeitlich aufwändig, periodisch alle existierenden Webseiten-URLs zu prüfen
    - Also müsste die automatische Prüfung intelligent sein:
      - Initial alle Webseiten-URLs prüfen
      - Danach nur noch jene Webseiten-URLs, deren Inhalt seit der letzten Prüfung verändert wurde

